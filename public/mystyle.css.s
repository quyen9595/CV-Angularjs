
*{font-family: 'Share Tech Mono';}

body {
   font-family: 'Share Tech Mono', ;
   background: #44A2C0;

}

h3{
	
	font-style: italic;
    color: white;
    text-align: center;
    font-size: 30px;
    margin: 0;
    margin-bottom: 20px;
	
}
li{
	color: white ;	
	
}

.myname h1{
	color: white;
	 margin: 0;
    padding-top: 10px;
    font-style: bold;
    display: block;
    border-bottom: solid 1px white;
    border-bottom-height: 10px;
    margin-bottom: 10px;
    width: 300px;
    text-align: center;

}


.container{
	/*padding-left: 100px;
	padding-right: 100px;*/

	 margin-left: 100px;
	 margin-right: 100px;

	
}
.main-content{
	width: 770px;
	float: left;
	
}
.right-bar{
	float: right;
    width: 350px;
}
.social-icon{
	padding-top: 10px;
	margin-left: 20px;
}
footer{
	padding-top: 5px;
	clear: both;
	background: #505050;
	color: white;
	text-align: center;

}

/*set background for each section*/
.intro{
	background: #FFC56F;
}
.hard-skill	{
	background: #877399;
}
.soft-skill{
	background: #48C383;
}
.experience{
	background: #C35C48;
}
.project{
	background: #7BC044;
	margin-bottom: 10px;
}

.intro, .hard-skill, .soft-skill, .experience, .project{
	padding-bottom: 20px;
	padding-top:20px;
}

/*reponsive*/
@media screen and (max-width: 900px) {
    body {
        background-color: black;
       
    }
    .main-content {
    	 width: 100%;

    }
    .container{
    	width: 100%;
    	margin:0;
    }
    .right-bar{
    	float: left;
    }
    header{
      width: 100%;
    }
    footer{
    	width: 100%;
    }

}
@media screen and (min-width: 900px) {
    body {
        /*background-color: black;
       */
    }
    .main-content {
    	 width: 50%;

    }
    .container{
    	width: 100%;
    	
    }
    .right-bar{
		/*width: 40%;*/
		float: left;
		margin-left: 10px;
    }
    header{
      width: 100%;
    }
    footer{
    	width: 100%;
    }

}